#　-*- coding:utf-8 -*-
import datetime
import ConfigClass
import re
import json
import scrapy
import web_text# 抽取正文的模块
from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.utils.url import urljoin_rfc

from bigghosthead.items import BigghostheadItem
from scrapy.loader.processors import MapCompose, Join
from scrapy.loader import ItemLoader
import sys
reload(sys)
sys.setdefaultencoding('utf8')

config = ConfigClass.Config()
class o_Ospder(Spider):
    name = "o_Ospider"
    allowed_domains = [
        "jd.com",
        "item.jd.com",
        "yp.jd.com",
        "club.jd.com",
        "www.jd.com",
        "mall.jd.com",
        "list.jd.com",
        "gongyi.jd.com",
        "sclub.jd.com"
    ]
    start_urls = [config.get_config('start_urls')]

    def parse(self, response):
        item = BigghostheadItem()
        sites = response.selector.xpath('//a/@href')
        for site in sites:
            print(site.extract())
            try:
                yield scrapy.Request(site.extract(), callback = self.parse, meta = {'http-equiv':"Content-Type", 'content':"text/html; charset=utf-8"})# 非绝对路径的那些不合法路径
            except:
                full_url = response.urljoin(site.extract())
                yield scrapy.Request(full_url, callback = self.parse)
        if 'J-hove-wrap EDropdown fr' in response.text: # 经过验证，class = J-hove-wrap EDropdown fr 只会存在于京东的商品详情页面，而不会出现在首页商铺页面。
            # 充填xpath语法参数
            re_id = re.compile('https://item.jd.com/(\d+?).html')
            productId = re_id.findall(response.url)[0]
            comment_url = "https://sclub.jd.com/comment/productPageComments.action?productId="+ productId + "&score=0&sortType=6&page=0&pageSize=10&isShadowSku=0&fold=1"
            yield scrapy.Request(comment_url, callback = self.parse_comment)
            item['productId'] = productId
            if config.get_config('xpath_title') != '':
                item['title'] = response.xpath(
                        config.get_config('xpath_title')
                    ).extract()[0].encode('utf-8')
            if config.get_config('xpath_store') != '':
                item['store'] = response.xpath(
                        config.get_config('xpath_store')
                    ).extract()[0].encode('utf-8')
            if config.get_config('xpath_comment')  != '':
                item['comment'] = response.xpath(
                        config.get_config('xpath_comment')
                    ).extract()
            if config.get_config('xpath_text')  != '':
                item['text'] = response.xpath(
                        config.get_config('xpath_text')
                    ).extract()
            if config.get_config('xpath_keywords')  != '':
                item['keywords'] = response.xpath(
                        config.get_config('xpath_keywords')
                    ).extract()                                           
        yield item

    def parse_comment(self, response):
        json_data = json.loads(response.text)
        for cd in json_data['comments']:
            item = CommentItem()
            item['comment'] = cd['content']
            item['productId'] = cd['referenceId']
            yield item
        if not json_data['comments']:
            retrun# 如果当前页数评论api的json中没有评论了，那么结束    
        re_page = re.compile('page=(\d+?)')
        page = re_page.findall(response.url)[0]
        a = 'page='+page
        b = 'page='+str(int(page)+1)
        next_comment_url = response.url.replace(a,b)
        yield scrapy.Request(next_comment_url, callback = self.parse_comment)