# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import MySQLdb
import sys
reload(sys)
sys.setdefaultencoding('utf8')

class BigghostheadPipeline(object):
    def process_item(self, item, spider):
        print(item)
        conn = MySQLdb.connect(user = 'root', passwd = 'root', host = '127.0.0.1', db = 'bigghosthead')
        cursor = conn.cursor()
        if item['title']:
            item['title'] = item['title'].encode("UTF-8")
            item['productId'] = item['productId'].encode("UTF-8")
            item['store'] = item['store'].encode("UTF-8")
            cursor.execute('insert into product(product, productId, store)values (%s,%s,%s)',[item['title'], item['productId'],item['store']])
            conn.close()
        elif item['comment']:
            item['comment'] = item['comment'].encode("UTF-8")
            item['productId'] = item['productId'].encode("UTF-8")
            cursor.execute('insert into comment(comment, productId)values (%s,%s)',[item['comment'], item['productId']])
            conn.close()
        return item
